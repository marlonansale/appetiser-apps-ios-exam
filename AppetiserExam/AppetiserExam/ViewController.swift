//
//  ViewController.swift
//  AppetiserExam
//
//  Created by MacPro on 18/03/2020.
//  Copyright © 2020 AppetiserApps. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

    @IBOutlet weak var lblHeader: UILabel!
    
    
    @IBOutlet weak var tableMusic: UITableView!
    var arrTrack = [TrackModel]()
    var hasLoaded = false
    
    let kLastUsed = "last_used"
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        checkLastUsed()
        fetchMusicTracks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if hasLoaded {
            ViewManager.shared.setView(currentView: .list, trackID: nil)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.barStyle = .black
    }
    
    
    
    // MARK: - Check User Last Used
    
    /*
        Adding first letter k to know that the variable is constant
        Using standard default to save the last date visited.
    */
    
   
    func checkLastUsed(){
       
        if let strDate =  UserDefaults.standard.string(forKey: kLastUsed) {
             self.lblHeader.text = "Last Visited \(strDate)"
        }else{
            self.lblHeader.text = "Welcome"
        }
      
        
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy hh:mm"
        
        let strDateNow = dateFormatter.string(from: Date())
        UserDefaults.standard.set(strDateNow, forKey: kLastUsed)
        UserDefaults.standard.synchronize()
    }

    
    // MARK: - Fetch Music from itunes
    func fetchMusicTracks() {
        
        TrackManager.shared.fetchMusicTracks(complete: { (arrTrack) in
            DispatchQueue.main.async {
                self.arrTrack = arrTrack
                self.tableMusic.reloadData()
                self.setupLastView()
                self.hasLoaded = true
                
            }
        }) { (fail) in
            DispatchQueue.main.async {
                self.alertError(error: fail)
            }
        }
    }
    
     // MARK: - Check Last View
    func setupLastView(){
        let lastView = UserDefaults.standard.integer(forKey: ViewManager.shared.kCurrentView)
        print("setupLastView \(lastView) ")
        if lastView == CurrentView.detail.rawValue {
            let trackID = UserDefaults.standard.integer(forKey: ViewManager.shared.kTrackID)
              print("setupLastView \(trackID) ")
            for track in self.arrTrack {
                
                if trackID == track.trackId {
                    print("setupLastView \(track) ")
                    pushToDetail(trackModel: track)
                    break
                }
            }
        }
        
    }
    
     // MARK: - Push to Detail
    func pushToDetail(trackModel:TrackModel){
        if let vc =       storyboard?.instantiateViewController(identifier: "MusicDetailViewController") as? MusicDetailViewController {
                   
                   vc.trackModel =  trackModel
                   
                   self.navigationController?.pushViewController(vc, animated: true)
               }
               
    }
    
    // MARK: - Alert Error
    func alertError(error:String){
        let alert = UIAlertController(title: "ERROR OCCUR:", message: error, preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { _ in
                       
                       }))
                       self.present(alert, animated: true, completion: nil)
    }


}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension ViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicTrackCell") as! MusicTrackCell
        
        let track = arrTrack[indexPath.row]
        
        cell.lblTrackName.text = track.trackName
        cell.lblGenre.text = track.genre
        cell.lblPrice.text = track.priceString()
        
        cell.imgView.sd_setImage(with: track.artWorkUrl(), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: SDWebImageOptions.refreshCached, context: nil)
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTrack.count
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        pushToDetail(trackModel: self.arrTrack[indexPath.row])
       
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UITableViewCell
class MusicTrackCell : UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
}
