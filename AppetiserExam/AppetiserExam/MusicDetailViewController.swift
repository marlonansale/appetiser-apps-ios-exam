//
//  MusicDetailViewController.swift
//  AppetiserExam
//
//  Created by MacPro on 18/03/2020.
//  Copyright © 2020 AppetiserApps. All rights reserved.
//

import UIKit
import SDWebImage

class MusicDetailViewController: UIViewController {

    var trackModel:TrackModel?
    
    @IBOutlet weak var tableDetail: UITableView!
    @IBOutlet var cellHead: UITableViewCell!
    @IBOutlet var cellDetail: UITableViewCell!
    @IBOutlet var cellBtn: UITableViewCell!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    
    @IBOutlet weak var txtViewDetails: UITextView!
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.barStyle = .black
    }
    
    
    // MARK: Setup View
    func setupDataView() {
        if let trackDetail = trackModel {
            ViewManager.shared.setView(currentView: .detail, trackID: trackDetail.trackId)
            
            lblTitle.text = trackDetail.trackName
            imgView.sd_setImage(with: trackDetail.artWorkUrl(), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: SDWebImageOptions.refreshCached, context: nil)
            lblArtist.text = "by \(trackDetail.artistName)"
            lblPrice.text = trackDetail.priceString()
            lblGenre.text = trackDetail.genre
            
            txtViewDetails.text = trackDetail.getMusicDetails()
            txtViewDetails.textContainerInset = UIEdgeInsets.zero
            txtViewDetails.sizeToFit()
            
        }
    }

    // MARK: Actions
    @IBAction func onAppleMusic(_ sender: Any) {
        if let trackDetail = trackModel, let url = trackDetail.trackURL() {
            UIApplication.shared.open(url, options: [:], completionHandler: { (complete) in
                                                                      
                      })
        }
    
        
    }
    
}

   // MARK: - UITableViewDataSource & UITableViewDelegate

   /* I prefer using table view rather than scrollview for a long view that needs scroll. Easy to edit or add something in the view */

extension MusicDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            return cellHead
        }else  if indexPath.row == 1{
            return cellDetail
        }else{
            return cellBtn
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 220
        }else if indexPath.row == 1 {
            return txtViewDetails.contentSize.height
        }else{
            return 100
        }
    }
    
}
