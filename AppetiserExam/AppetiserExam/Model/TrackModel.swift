//
//  MusicModel.swift
//  AppetiserExam
//
//  Created by MacPro on 18/03/2020.
//  Copyright © 2020 AppetiserApps. All rights reserved.
//

import Foundation

struct TrackModel {
    
    let trackName:String
    let artWorkThumb:String
    let artWork:String
    let genre:String
    let trackPrice:Double
    let collectionPrice:Double
    let trackId:Int
    let artistName:String
    
    let collectionName:String
    let trackViewUrl:String
    
    var releaseDate:Date?
    
    
    init(dictionary:Dictionary<String,Any>) {
        trackName = dictionary["trackName"] as? String ?? ""
        trackId = dictionary["trackId"] as? Int ?? 0
        
        trackPrice = dictionary["trackPrice"] as? Double ?? 0
        collectionPrice = dictionary["collectionPrice"] as? Double ?? 0
        
        genre =  dictionary["primaryGenreName"] as? String ?? ""
        artWorkThumb = dictionary["artworkUrl60"] as? String ?? ""
        artWork = dictionary["artworkUrl100"] as? String ?? ""
        artistName =  dictionary["artistName"] as? String ?? ""
        
        collectionName =  dictionary["collectionName"] as? String ?? ""
        
        trackViewUrl = dictionary["trackViewUrl"] as? String ?? ""
     
        if let strDate = dictionary["releaseDate"] as? String  {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            
          
            releaseDate = dateFormatter.date(from: strDate)
        }
        
    }
    
    
    func priceString() -> String {
        if trackPrice == 0 {
                 return "N/A"
            }else{
                 return "$\(String(format: "%.2f", trackPrice as CVarArg))"
            }
    }
    
    
    func collectionPriceString() -> String {
        if collectionPrice == 0 {
                 return "N/A"
            }else{
                 return "$\(String(format: "%.2f", collectionPrice as CVarArg))"
            }
    }
    func artWorkUrl() -> URL? {
        return URL.init(string: artWork)
    }
    
    
    func releaseDateString() -> String {
        
        if let date = releaseDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM dd, yyyy"
            
            return dateFormatter.string(from: date)
        }else{
            return "N/A"
        }
    }
    
    func trackURL() -> URL? {
        return URL.init(string: trackViewUrl)
    }
    
    func getMusicDetails() -> String {
        
        return "\(trackName) is a song by \(artistName) from the album \(collectionName). It was release on \(releaseDateString()). Album price is \(collectionPriceString()) and Track price is \(priceString())"
    }
}

