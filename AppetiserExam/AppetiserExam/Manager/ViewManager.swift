//
//  ViewManager.swift
//  AppetiserExam
//
//  Created by MacPro on 19/03/2020.
//  Copyright © 2020 AppetiserApps. All rights reserved.
//

import UIKit


enum CurrentView:Int {
    case list = 0
    case detail = 1
}

class ViewManager {
    
    let kCurrentView = "current_view"
    let kTrackID = "track_id"
    static let shared = ViewManager()
    
    func setView(currentView:CurrentView, trackID:Int?){
        
        UserDefaults.standard.set(currentView.rawValue, forKey: kCurrentView)
        
        if let trackId = trackID, currentView == .detail {
          

            UserDefaults.standard.set(trackId, forKey: kTrackID)
          
            
        }
        UserDefaults.standard.synchronize()
    }
    
    
    
    
}
