//
//  MusicManager.swift
//  AppetiserExam
//
//  Created by MacPro on 18/03/2020.
//  Copyright © 2020 AppetiserApps. All rights reserved.
//

import Foundation

// Completion Handler for returning the request
typealias CompletionHandler = (_ success:[TrackModel]) -> Void
typealias FailHandler = (_ fail:String) -> Void

// Manager use to fetch data from web
class TrackManager {
    static let shared = TrackManager()

    
    // I can also work with third parties URL Fetcher like Alamofire.
    func fetchMusicTracks(complete:@escaping CompletionHandler, fail:@escaping FailHandler) {
            //let url = "https://randomuser.me/api/?results=30"
            let url = "https://itunes.apple.com/lookup?id=909253&entity=song"
        
            let urlData = URL(string:url)!
            var urlrequest:URLRequest = URLRequest(url: urlData)
            urlrequest.httpMethod = "GET"
             
            urlrequest.allHTTPHeaderFields = ["Content-Type":"application/json"]
        
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = TimeInterval(30)
            config.timeoutIntervalForResource = TimeInterval(30)
            let urlSession = URLSession(configuration: config)
        
            let task = urlSession.dataTask(with: urlrequest) { (data, response, error) in
                
                
                if let err = error {
                                  fail(err.localizedDescription)
                }else if let httpResponse = response as? HTTPURLResponse {
                    
                     guard let dataDetail = data else {
                         fail("Unexpected Error Occur.")
                        return
                    }
                    
                   if (httpResponse.statusCode == 200 || httpResponse.statusCode == 201) {
                        do {
                            if let dictionary = try JSONSerialization.jsonObject(with: dataDetail  , options: .mutableContainers) as? Dictionary<String,Any>, let results =  dictionary["results"] as? [Dictionary<String,Any>] {
                               
                                var arrMusic = [TrackModel]()
                                
                                for wrapper in results {
                                    if let wrapperType = wrapper["wrapperType"] as? String, wrapperType == "track"{
                                          arrMusic.append(TrackModel(dictionary: wrapper))
                                    }
                                  
                                }
                                
                                complete(arrMusic)
                            }else{
                                fail("Unexpected Error Occured.")
                            }
                        
                        }catch{
                             fail("Unexpected Error Occured.")
                        }
                         
                    
                    }else{
                        fail("Error code: \(httpResponse.statusCode)")
                    }
                }
            }
        
         task.resume()
    }
    
}

